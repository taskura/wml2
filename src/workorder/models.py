from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import pre_save, post_save
from datetime import datetime, timedelta
from django.utils import timezone
# Create your models here.
import inspect

def current_user():
	for frame_record in inspect.stack():
		if frame_record[3]=='get_response':
			request = frame_record[0].f_locals['request']
			break
		else:
			request = None
	return request.user

JOB_STATUS = (
	("In Progress","In Progress"),
	("Quoting","Quoting"),
	("New Job","New Job"),
	("Completed","Completed"),
	("Invoicing","Invoicing"),
	("Closed","Closed")
	)

TASK_STATUS = (
	("Not Started","Not Started"),
	("Waiting for Parts","Waiting for Parts"),
	("Waiting for Labour","Waiting for Labour"),
	("Completed","Completed"),
	("In Progress","In Progress"),
	("On-Hold","On-Hold")
	)

PO_STATUS = (
	("Closed","Closed"),
	("New","New"),
	("Order Sent","Order Sent"),
	("Waiting Receipts","Waiting Receipts"),
	("Fully Receipted","Fully Receipted")
	)

INVOICE_STATUS = (
	("Sent","Sent"),
	("Paid","Paid")
	)

QUOTE_STATUS = (
	("Quoting","Quoting"),
	("Quote Sent","Quote Sent"),
	("Approved","Approved"),
	("Rejected","Rejected")
	)


class DocumentationCategory(models.Model):
	title = models.CharField(max_length=100)
	def __str__(self):
		return self.title

class Customer(models.Model):
	customer_name 	  = models.CharField(max_length = 100)
	customer_abn 	  = models.IntegerField(null=True, blank=True)
	customer_contact  = models.CharField(max_length = 50)
	customer_email 	  = models.EmailField(max_length=254)
	customer_address  = models.TextField()
	customer_number   = models.IntegerField()
	customer_contact1 = models.CharField(max_length = 200, null=True, blank=True)
	customer_address1 = models.TextField(null=True, blank=True)
	customer_number1  = models.IntegerField(null=True, blank=True)
	is_vendor		  = models.BooleanField(default = False)

	def __str__(self):
		return self.customer_name


class Quote(models.Model):
	customer 				= models.ForeignKey(Customer,on_delete=models.CASCADE)
	description 			= models.TextField()
	custom_quote 			= models.CharField(max_length=100, unique=True)
	description_long_text 	= models.TextField()
	customer_contact_date 	= models.DateTimeField()
	quote_request_date 		= models.DateTimeField()
	promised_delivery_date  = models.DateTimeField()
	quote_sent_date 		= models.DateTimeField(null=True,blank=True)
	created_date 			= models.DateTimeField(auto_now = True,auto_now_add = False)
	modified_date 			= models.DateTimeField(auto_now = False,auto_now_add = True)
	quote_amount            = models.FloatField(default=0)
	quote_status            = models.CharField(max_length=100, choices=QUOTE_STATUS, default="Quoting")

	def __str__(self):
		return self.description

class QuoteDocument(models.Model):
	quote = models.ForeignKey(Quote,on_delete=models.CASCADE)
	category = models.ForeignKey(DocumentationCategory, on_delete=models.CASCADE)
	document   = models.FileField(upload_to = "quote_documentation")

	def __str__(self):
		return "%s"%(self.id)


def quot_status_change_receiver(sender, instance, *args, **kwargs):
	status = instance.quote_status
	if status == "Quote Sent":
		if not instance.quote_sent_date:
			instance.quote_sent_date = timezone.now()
	if status == "Approved":
		try:
			work_order = instance.workorder
		except Exception as e:
			work_order = None
		if not work_order:
			WorkOrder.objects.create(
			customer = instance.customer,
			description = instance.description,
			description_long_text = instance.description_long_text,
			quote = instance,
			customer_contact_date = instance.customer_contact_date,
			promised_delivery_date = instance.promised_delivery_date,
			quote_sent_date = instance.quote_sent_date
			)

pre_save.connect(quot_status_change_receiver, sender=Quote)



class WorkOrder(models.Model):
	customer 				= models.ForeignKey(Customer,on_delete=models.CASCADE)
	description 			= models.TextField()
	description_long_text 	= models.TextField(null=True,blank=True)
	initial_deu_date 		= models.DateTimeField(null=True,blank=True)
	quote 					= models.OneToOneField(Quote, null=True, blank=True)
	custom_quote 			= models.CharField(max_length=100,unique=True)
	customer_contact_date 	= models.DateTimeField()
	quote_sent_date 		= models.DateTimeField()
	revision 				= models.FloatField(default=1.0)
	promised_delivery_date  = models.DateTimeField()
	created_by 				= models.ForeignKey(User,on_delete=models.CASCADE,related_name='created_by',null=True,blank=True)
	created_date 			= models.DateTimeField(auto_now = True,auto_now_add = False)
	modified_by 			= models.ForeignKey(User,on_delete=models.CASCADE,related_name='modified_by',null=True,blank=True)
	modified_date 			= models.DateTimeField(auto_now = False,auto_now_add = True)
	job_status 				= models.CharField(max_length=100, choices=JOB_STATUS, default="Quoting")
	all_tasks_complete 		= models.BooleanField(default = False)
	is_template 			= models.BooleanField(default = False)


	def __str__(self):
		return self.description






class Documentation(models.Model):
	work_order = models.ForeignKey(WorkOrder,on_delete=models.CASCADE)
	category = models.ForeignKey(DocumentationCategory, on_delete=models.CASCADE, blank=True,null=True)
	document   = models.FileField(upload_to = "documentation", blank=True,null=True)

	def __str__(self):
		return "%s"%(self.id)







class Employee(models.Model):
	user                           = models.OneToOneField(User,null=True,blank=True, related_name="em_user")
	external_system 			   = models.PositiveIntegerField(null=True,blank=True)
	name  						   = models.CharField(max_length=100)
	email 	                       = models.EmailField(null=True,blank=True)
	start_date 					   = models.DateTimeField(null=True,blank=True)
	terminate_date 				   = models.DateTimeField(null=True,blank=True)
	terminated 					   = models.BooleanField(default=False)
	photo 						   = models.ImageField(upload_to="employee",null=True,blank=True)
	address 					   = models.TextField(null=True,blank=True)
	phone_number 				   = models.IntegerField(null=True,blank=True)
	emergency_contact 			   = models.CharField(max_length = 50, null=True,blank=True)
	emergency_contact_relationship = models.CharField(max_length = 50, null=True,blank=True)
	emergency_contact_number 	   = models.IntegerField(null=True,blank=True)
	emergency_contact_email 	   = models.EmailField(null=True,blank=True)
	cost_rate 					   = models.FloatField(default=0)
	chargeout_rate 				   = models.FloatField(default=0)

	def __str__(self):
		return self.name


def terminated_receiver(sender, instance, *args, **kwargs):
	if instance.terminate_date:
		instance.terminated = True
	else:
		instance.terminated = False

pre_save.connect(terminated_receiver, sender=Employee)


class Task(models.Model):
	work_order 		 = models.ForeignKey(WorkOrder,on_delete=models.CASCADE)
	task_description = models.TextField(null=True,blank=True)
	start_date       = models.DateTimeField(null=True,blank=True)
	end_date         = models.DateTimeField(null=True,blank=True)
	task_status 	 = models.CharField(max_length=100, choices=TASK_STATUS, default="In Progress")
	task_duration 	 = models.IntegerField(default=0)
	task_assigned_to = models.ManyToManyField(Employee, blank=True)
	task_due_date 	 = models.DateTimeField(null=True,blank=True)

	def __str__(self):
		return self.task_description

def task_duration_receiver(sender, instance, *args, **kwargs):
    day_between = instance.end_date-instance.start_date
    passed_hour = day_between.days * 24 + day_between.seconds // 3600
    instance.task_duration = int(passed_hour)

pre_save.connect(task_duration_receiver, sender=Task)


class TimeCard(models.Model):
	work_order 	  = models.ForeignKey(WorkOrder,on_delete=models.CASCADE)
	employee_name = models.ForeignKey(Employee,on_delete=models.CASCADE)
	start_time 	  = models.DateTimeField()
	end_time      = models.DateTimeField()
	duration      = models.FloatField(default=0)
	approved      = models.BooleanField(default = False)
	approved_date = models.DateTimeField(null=True,blank=True)

	gst = models.FloatField(default=0)
	total_cost = models.FloatField(default=0)
	mp = models.FloatField(default=0)
	ma = models.FloatField(default=0)

	def __str__(self):
		return self.employee_name.name

	def get_total_ex_gst(self):
		duration = self.duration
		chargeout_rate = self.employee_name.chargeout_rate
		markup_percentage = self.mp
		markup_amount = self.ma
		line_total_ex_gst = duration * chargeout_rate * (1+markup_percentage/100) + markup_amount
		return line_total_ex_gst

class Purchase(models.Model):
	work_order 				= models.ForeignKey(WorkOrder,on_delete=models.CASCADE,null=True,blank=True)
	po_status 			  	= models.CharField(max_length=100, choices=PO_STATUS, default="New")
	vendor					= models.ForeignKey(Customer,on_delete=models.CASCADE)
	po_cost 				= models.FloatField(default=0)
	ordered_date 		 	= models.DateTimeField(auto_now = True,auto_now_add = False)
	required_date 		 	= models.DateTimeField()
	promised_delivery_date	= models.DateTimeField(null=True,blank=True)
	approved 			  	= models.BooleanField(default=False)

	def __str__(self):
		return self.work_order.description




class Item(models.Model):
	purchase 	= models.ForeignKey(Purchase,on_delete=models.CASCADE)
	line_text = models.CharField(max_length=200)
	quantity = models.PositiveIntegerField(default=1)
	received_qty = models.PositiveIntegerField(default=1,null=True,blank=True)
	received_date = models.DateTimeField(null=True,blank=True)
	po_cost_ex_gst = models.FloatField(default=0)
	gst = models.FloatField(default=0)
	line_cost 	= models.FloatField(default=0)
	approved = models.BooleanField(default=False)
	mp = models.FloatField(default=0)
	ma = models.FloatField(default=0)
	is_markuped = models.BooleanField(default=False)

	def __str__(self):
		return self.line_text

	def get_total_ex_gst(self):
		qty = self.quantity
		cost = self.po_cost_ex_gst
		markup_percentage = self.mp
		markup_amount = self.ma
		line_total_ex_gst = qty * cost * (1+markup_percentage/100) + markup_amount
		return line_total_ex_gst




class Invoice(models.Model):
	work_order						= models.OneToOneField(WorkOrder,on_delete=models.CASCADE)
	external_account_code 			= models.CharField(max_length=100,null=True, blank=True)
	invoice_date 					= models.DateTimeField(null=True, blank=True)
	expected_payment_date 			= models.DateTimeField(null=True, blank=True)
	invoice_status 					= models.CharField(max_length=100, choices=INVOICE_STATUS,null=True, blank=True)
	email_to 						= models.EmailField(max_length = 100,null=True, blank=True)
	description 					= models.TextField(null=True, blank=True)
	payment_details 				= models.TextField(null=True, blank=True)
	invoice_to 						= models.TextField(null=True, blank=True)
	invoice_from_org 				= models.TextField(null=True, blank=True)
	invoice_from 					= models.TextField(null=True, blank=True)
	invoice_customer 				= models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, blank=True)
	invoice_customer_info 			= models.TextField(null=True, blank=True)
	is_created                      = models.BooleanField(default=False)
	send_copy_tick_box 				= models.BooleanField(default = False)
	add_pdf_attachment_tick_box 	= models.BooleanField(default = False)
	add_invoice_attachment_to_email = models.BooleanField(default = False)
	approved                        = models.BooleanField(default=False)

	def __str__(self):
		return self.work_order.description

to_form = '''
{}
{}
'''

def invoice_create_receiver(sender, instance, created, *args, **kwargs):
	if created:
		ito = to_form.format(instance.customer.customer_name, instance.customer.customer_address).strip()
		icf = to_form.format(instance.customer.customer_name, instance.customer.customer_email).strip()
		Invoice.objects.create(
			work_order = instance,
			email_to = instance.customer.customer_email,
			invoice_customer = instance.customer,
			invoice_to = ito,
			invoice_customer_info = icf
			)

post_save.connect(invoice_create_receiver, sender=WorkOrder)

def deu_date_receiver(sender, instance, *args, **kwargs):
	if instance.job_status == "Quoting" or instance.job_status == "New Job":
		pdd = instance.promised_delivery_date
		instance.initial_deu_date = pdd
pre_save.connect(deu_date_receiver, sender=WorkOrder)


class Preference(models.Model):
	user = models.OneToOneField(User)
	logo = models.ImageField(upload_to="invoice_logo", null=True,blank=True)
	address = models.TextField(null=True,blank=True)
	markup_percentage = models.FloatField(default=0)
	markup_amount = models.FloatField(default=0)

	def __str__(self):
		return self.user.username


def preference_receiver(sender,created, instance, *args, **kwargs):
	if created:
		Preference.objects.create(
			user = instance
			)
post_save.connect(preference_receiver, sender=User)
