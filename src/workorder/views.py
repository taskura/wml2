from django.shortcuts import render,get_object_or_404,redirect
from django.template.loader import render_to_string
from django.http import JsonResponse
import datetime
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from .models import (
	WorkOrder,
	Employee,
	Customer,
	Purchase,
	Task,
	TimeCard,
	Documentation,
	Invoice,Item,
	Preference,
	JOB_STATUS,
	QUOTE_STATUS,
	Quote
	)

from .forms import (
	WorkOrderForm,
	EmployeeForm,
	EmployeeUpdateForm,
	CustomerForm,
	PurchaseForm,
	TaskForm,
	TaskItemInlineForm,
	TimeCardForm,
	DocumentationForm,
	InvoiceForm,
	PurchaseItemInlineForm,
	QuoteForm,
	QuoteUpdateForm,
	DocumentationInlineForm,
	QuoteDocumentationInlineForm
	)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView,ListView,DetailView,UpdateView
from django.db import transaction
from django.core.urlresolvers import resolve
from django.http import JsonResponse, HttpResponse
from django.utils import timezone
from django.db.models import Q
# Create your views here.

def Home(request):
	return render(request, 'home.html')


@login_required
def workorder_list(request):
	closex = ['Closed','Invoicing','Completed']
	workorders = WorkOrder.objects.all()
	open_order = WorkOrder.objects.exclude(job_status__in=closex).count()
	today = datetime.datetime.today().date()
	this_week = [today + datetime.timedelta(days=i) for i in range(0 - today.weekday(), 7 - today.weekday())]
	deu_date = WorkOrder.objects.filter(initial_deu_date__in=this_week).count()
	overdeu_date = WorkOrder.objects.filter(initial_deu_date__lt=today).count()

	current_url = request.build_absolute_uri()
	if "open_work_order" in current_url:
		workorders = WorkOrder.objects.exclude(job_status__in=closex)

	if "work_orders_due_this_week" in current_url:
		workorders = WorkOrder.objects.filter(initial_deu_date__in=this_week)

	if "work_orders_overdue" in current_url:
		workorders = WorkOrder.objects.filter(initial_deu_date__lt=today)
	#send_copy_tick_box

	context = {

		"workorders":workorders,
		"work_form":WorkOrderForm(),
		"open_order":open_order,
		"deu_date":deu_date,
		"overdeu_date":overdeu_date,
		"job_status":JOB_STATUS,
	}

	return render(request,'workorder/workorder_list.html',context)

def workorder_status_update(request,id):
	if request.method == "POST":
		new_status = request.POST['new_status']
		workorder = WorkOrder.objects.get(id=id)
		workorder.job_status = new_status
		workorder.save()

	return HttpResponse("")

def quote_status_update(request,id):
	quote = Quote.objects.get(id=id)
	if request.method == "POST":
		new_status = request.POST['new_status']
		if new_status == "Quote Sent":
			if not quote.quote_sent_date:
				quote.quote_sent_date = timezone.now()
		if new_status == "Approved":
			try:
				work_order = quote.workorder
			except Exception as e:
				work_order = None
			if not work_order:
				WorkOrder.objects.create(
				customer = quote.customer,
				description = quote.description,
				description_long_text = quote.description_long_text,
				quote = quote,
				custom_quote = quote.custom_quote,
				customer_contact_date = quote.customer_contact_date,
				promised_delivery_date = quote.promised_delivery_date,
				quote_sent_date = quote.quote_sent_date
				)
		quote.quote_status = new_status
		quote.save()

	return HttpResponse("")

def workorder_description_update(request,id):
	if request.method == "POST":
		new_text = request.POST['new_text']
		workorder = WorkOrder.objects.get(id=id)
		workorder.description = new_text
		workorder.save()

	return HttpResponse("")

def errors_to_json(errors):
	"""
	Convert a Form error list to JSON::
	"""
	return dict(
	(k, map(unicode, v))
	for (k,v) in errors.iteritems()
	)



class WorkOrderCreate(LoginRequiredMixin, CreateView):
	model = WorkOrder
	template_name = 'workorder/workorder_create.html'
	form_class = WorkOrderForm
	success_url = '/workorder/list/'

	def form_valid(self, form, *args, **kwargs):
		form.instance.created_by = self.request.user
		form.instance.save()
		return super(WorkOrderCreate, self).form_valid(form, *args, **kwargs)

	def form_invalid(self, form, *args, **kwargs):
		return JsonResponse(form.errors)

class WorkOrderDetail(LoginRequiredMixin, DetailView):
	model = WorkOrder
	template_name = 'workorder/workorder_details.html'
	context_object_name = 'workorder_details'

class PreferenceDetails(LoginRequiredMixin, UpdateView):
	model = Preference
	template_name = 'preference.html'
	fields = ['logo','address','markup_percentage','markup_amount']
	success_url = '/workorder/list/'



class EmployeeList(LoginRequiredMixin, ListView):
    model = Employee
    template_name = 'employee/employee_list.html'
    context_object_name = 'employee_list'

    def get_context_data(self,**kwargs):
        data = super(EmployeeList,self).get_context_data(**kwargs)
        eul = []
        for eu in Employee.objects.all():
            if eu.user:
               eul.append(eu.user.id)

        users = User.objects.exclude(
			Q(id__in=eul)|
			Q(id=self.request.user.id)
			)
        data["users"] = users
        data["employee_form"] = EmployeeForm()
        return data



class EmployeeCreate(LoginRequiredMixin, CreateView):
	model = Employee
	template_name = 'employee/employee_create.html'
	form_class = EmployeeForm
	success_url = '/employee/list/'




class CustomerList(LoginRequiredMixin, ListView):
	model = Customer
	template_name = 'customer/customer_list.html'
	context_object_name = 'customer_list'

	def get_context_data(self,**kwargs):
		data = super(CustomerList,self).get_context_data(**kwargs)
		data["customer_form"] = CustomerForm()
		return data



class CustomerCreate(LoginRequiredMixin, CreateView):
	model = Customer
	template_name = 'customer/customer_create.html'
	form_class = CustomerForm
	success_url = '/customer/list/'



class PurchaseList(LoginRequiredMixin, ListView):
	model = Purchase
	template_name = 'purchase/purchase_list.html'
	context_object_name = 'purchase_list'

	def get_context_data(self,**kwargs):
		pk = self.kwargs.get('pk')
		data = super(PurchaseList,self).get_context_data(**kwargs)
		form1 = PurchaseForm
		form2 = PurchaseItemInlineForm
		po_not_received_yet = 0

		# if "work_orders_overdue" in current_url:

		data["form1"] = form1
		data["form2"] = form2
		data["PO_not_approved"] = Purchase.objects.filter(approved=False).count()
		data["PO_approved"] = Purchase.objects.filter(approved=True).count()
		data["total_qty"] = po_not_received_yet
		data["items"] = Item.objects.all()
		return data

	def get_queryset(self):
		current_url = self.request.build_absolute_uri()
		qs = Purchase.objects.all()
		if "po_not_approved" in current_url:
			qs = Purchase.objects.filter(approved=False)
		if "po_approved" in current_url:
			qs = Purchase.objects.filter(approved=True)
		# if "po_not_received_yet" in current_url:
		# 	qs = Purchase.objects.filter(approved=False).count()

		return qs

def purchase_promised_delivery_date_update(request,id):
	if request.method == "POST":
		purchase = Purchase.objects.get(id=id)
		new_date = request.POST['new_date']
		purchase.promised_delivery_date = new_date
		purchase.save()
	return HttpResponse("")



class TaskCreate(CreateView):
	model = Task
	template_name = 'task/task_create.html'
	form_class = TaskForm
	success_url = '/workorder/list/'



class TimeCardList(ListView):
	model = TimeCard
	template_name = 'timecard/timecard_list.html'
	context_object_name = 'timecard_list'

	def get_context_data(self,**kwargs):
		data = super(TimeCardList,self).get_context_data(**kwargs)
		data["timecard_form"] = TimeCardForm()
		data["not_approved"] = TimeCard.objects.filter(approved=False).count()
		data["duration"] = TimeCard.objects.filter(approved=False,duration__gt=8).count()
		return data

	def get_queryset(self):
		current_url = self.request.build_absolute_uri()
		qs = TimeCard.objects.all()
		if "open_time_cards_not_approved" in current_url:
			qs = TimeCard.objects.filter(approved=False)
		if "employee_time_cards_not_approved_gt_8_duration" in current_url:
			qs = TimeCard.objects.filter(approved=False,duration__gt=8)
		return qs

class TimeCardCreate(CreateView):
	model = TimeCard
	template_name = 'timecard/timecard_create.html'
	form_class = TimeCardForm
	success_url = '/timecard/list/'

	def form_valid(self, form, *args, **kwargs):
		markup_percentage = self.request.user.preference.markup_percentage
		markup_amount = self.request.user.preference.markup_amount
		markup_gst = 10
		day_between = form.instance.end_time - form.instance.start_time
		duration = day_between.days * 24 + day_between.seconds // 3600
		chargeout_rate = form.instance.employee_name.chargeout_rate
		total = duration * chargeout_rate * (1+markup_percentage/100) + markup_amount
		gst = total / 100 * markup_gst
		line_total = total + gst

		form.instance.duration = duration
		form.instance.total_cost = line_total
		form.instance.mp = markup_percentage
		form.instance.ma = markup_amount
		form.instance.gst = gst
		form.instance.save()
		return super(TimeCardCreate, self).form_valid(form, *args, **kwargs)


class DocumentationList(ListView):
	model = Documentation
	template_name = 'documentation/documentation_list.html'
	context_object_name = 'documentation_list'

	def get_context_data(self,**kwargs):
		data = super(DocumentationList,self).get_context_data(**kwargs)
		data["documentation_form"] = DocumentationForm()
		return data


class DocumentationCreate(CreateView):
	model = Documentation
	template_name = 'documentation/documentation_create.html'
	form_class = DocumentationForm
	success_url = '/documentation/list/'




def wml_render(request, form, template_name,redirect_to):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            doc_form = form.save()
            data['form_is_valid'] = True
            return redirect(redirect_to)

        else:
            data['form_is_valid'] = False
    eul = []
    for eu in Employee.objects.all():
        if eu.user:
           eul.append(eu.user.id)

    users = User.objects.exclude(
		Q(id__in=eul)|
		Q(id=request.user.id)
		)
    context = {
		'form': form,
		'users':users,
		}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def document_update(request, pk):
    doc = get_object_or_404(Documentation, pk=pk)
    if request.method == 'POST':
        form = DocumentationForm(request.POST,request.FILES or None, instance=doc)
    else:
        form = DocumentationForm(instance=doc)
    return wml_render(request, form, 'documentation/update_documentation.html','/documentation/list/')



def timecard_render(request, form, template_name,redirect_to):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form = form.save()
            markup_percentage = form.mp
            markup_amount = form.ma
            markup_gst = 10
            day_between = form.end_time - form.start_time
            duration = day_between.days * 24 + day_between.seconds // 3600
            chargeout_rate = form.employee_name.chargeout_rate
            total = duration * chargeout_rate * (1+markup_percentage/100) + markup_amount
            gst = total / 100 * markup_gst
            line_total = total + gst

            form.duration = duration
            form.total_cost = line_total
            form.mp = markup_percentage
            form.ma = markup_amount
            form.gst = gst
            form.save()

            return redirect(redirect_to)

        else:
            data['form_is_valid'] = False
    context = {'form': form,}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def timecard_update(request, pk):
    timecard = get_object_or_404(TimeCard, pk=pk)
    if request.method == 'POST':
        form = TimeCardForm(request.POST,request.FILES or None, instance=timecard)
    else:
        form = TimeCardForm(instance=timecard)
    return timecard_render(request, form, 'timecard/update_timecard.html','/timecard/list/')





def customer_update(request, pk):
    customer = get_object_or_404(Customer, pk=pk)
    if request.method == 'POST':
        form = CustomerForm(request.POST,request.FILES or None, instance=customer)
    else:
        form = CustomerForm(instance=customer)
    return wml_render(request, form, 'customer/update_customer.html','/customer/list/')


def employee_update(request, pk):
    employee = get_object_or_404(Employee, pk=pk)
    if request.method == 'POST':
        form = EmployeeUpdateForm(request.POST,request.FILES or None, instance=employee)
    else:
        form = EmployeeUpdateForm(instance=employee)
    return wml_render(request, form, 'employee/update_employee.html','/employee/list/')

from django.db import transaction

def workorder_render(request, form,task_form,document_form, template_name,redirect_to,pk):
    data = dict()

    timecards = TimeCard.objects.filter(work_order=pk)
    tasks = Task.objects.filter(work_order=pk)
    purchases = Purchase.objects.filter(work_order=pk)
    items = Item.objects.filter(purchase__work_order=pk)
    documentation = Documentation.objects.filter(work_order=pk)
    redirect_url = redirect_to+"?workworder-"+pk
    if request.method == 'POST':
        if form.is_valid() and task_form.is_valid() and document_form.is_valid():
            doc_form = form.save()
            with transaction.atomic():
                task_form.instance = doc_form
                document_form.instance = doc_form
                task_form.save()
                document_form.save()
            return redirect(redirect_url)

        else:
          # pass
            # data['form_is_valid'] = False
            data['err'] = document_form.errors
            print(document_form.errors)
            print(task_form.errors)
            # return redirect(redirect_url)
    context = {
		'form': form,
		'task_form':task_form,
		'document_form':document_form,
		'timecards':timecards,
		'tasks':tasks,
		'purchases':purchases,
		'items':items,
		'documentation':documentation,
    }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)



def workorder_update(request, pk):
	workorder = get_object_or_404(WorkOrder, pk=pk)
	if request.method == 'POST':
		form = WorkOrderForm(request.POST,request.FILES or None, instance=workorder)
		task_form = TaskItemInlineForm(request.POST, instance=workorder)
		document_form = DocumentationInlineForm(request.POST,request.FILES or None, instance=workorder)

	else:
		form = WorkOrderForm(instance=workorder)
		task_form = TaskItemInlineForm(instance=workorder)
		document_form = DocumentationInlineForm(instance=workorder)

	return workorder_render(request, form,task_form,document_form, 'workorder/update_workorder.html','/workorder/list/',pk)


def purchase_update_render(request, form1,form2, template_name,redirect_to):
    data = dict()
    if request.method == 'POST':
        if form1.is_valid() and form2.is_valid():
            form1 = form1.save()
            po_cost = 0
            with transaction.atomic():
                form2.instance = form1
                items = form2.save()
                markup_gst = 10
                for item in items:
                    if item.is_markuped:
                        markup_percentage = item.mp
                        markup_amount = item.ma
                    else:
                        markup_percentage = request.user.preference.markup_percentage
                        markup_amount = request.user.preference.markup_amount
                        item.is_markuped = True
                    qty = item.quantity
                    cost = item.po_cost_ex_gst
                    line_total_ex_gst = qty * cost * (1+markup_percentage/100) + markup_amount
                    gst = line_total_ex_gst / 100 * markup_gst
                    item.gst = gst
                    item.line_cost = line_total_ex_gst + gst
                    item.ma = markup_amount
                    item.mp = markup_percentage
                    item.save()

                po_items = Item.objects.filter(purchase=form1)
                for item in po_items:
                	po_cost = po_cost + item.line_cost
                form2.instance.po_cost = po_cost
                form2.instance.save()


        return redirect(redirect_to)
    context = {
		'form1': form1,
		'form2': form2,
    }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

def purchase_create_render(request, form1,form2, template_name,redirect_to):
    data = dict()
    if request.method == 'POST':
        if form1.is_valid() and form2.is_valid():
            form1 = form1.save()
            markup_percentage = request.user.preference.markup_percentage
            markup_amount = request.user.preference.markup_amount
            markup_gst = 10
            po_cost = 0
            with transaction.atomic():
                form2.instance = form1
                items = form2.save()
                for item in items:
                    qty = item.quantity
                    cost = item.po_cost_ex_gst
                    line_total_ex_gst = qty * cost * (1+markup_percentage/100) + markup_amount
                    gst = line_total_ex_gst / 100 * markup_gst
                    line_total = line_total_ex_gst + gst
                    po_cost = po_cost + line_total
                    item.gst = gst
                    item.line_cost = line_total
                    item.ma = markup_amount
                    item.mp = markup_percentage
                    item.is_markuped = True
                    item.save()
                form2.instance.po_cost = po_cost
                form2.instance.save()


        return redirect(redirect_to)
    context = {
		'form1': form1,
		'form2': form2,
    }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
def PurchaseCreate(request):
    if request.method == 'POST':
        form1 = PurchaseForm(request.POST)
        form2 = PurchaseItemInlineForm(request.POST)
    else:
        form1 = PurchaseForm()
        form2 = PurchaseItemInlineForm()
    return purchase_create_render(request, form1,form2, 'purchase/purchase_create.html','/purchase/list/')


@login_required
def purchase_update(request, pk):
    purchase = get_object_or_404(Purchase, pk=pk)
    if request.method == 'POST':
        form1 = PurchaseForm(request.POST, instance=purchase)
        form2 = PurchaseItemInlineForm(request.POST, instance=purchase)
    else:
        form1 = PurchaseForm(instance=purchase)
        form2 = PurchaseItemInlineForm(instance=purchase)
    return purchase_update_render(request, form1,form2, 'purchase/update_purchase.html','/purchase/list/')






class InvoiceList(LoginRequiredMixin, ListView):
	model = Invoice
	template_name = 'invoice/invoice_list.html'
	context_object_name = 'invoice_list'

	def get_context_data(self,**kwargs):
		data = super(InvoiceList,self).get_context_data(**kwargs)
		data["not_approved"] = Invoice.objects.filter(approved=False).count()
		data["inv_not_sent"] = Invoice.objects.filter(send_copy_tick_box=False,approved=True).count()
		return data

	def get_queryset(self):
		current_url = self.request.build_absolute_uri()
		qs = Invoice.objects.all()
		if "invoice_requiring_approval" in current_url:
			qs = Invoice.objects.filter(approved=False)
		if "po_invoice_not_sent_but_approved" in current_url:
			qs = Invoice.objects.filter(send_copy_tick_box=False,approved=True)
		return qs


def invoice_render(request, form, template_name,redirect_to,invoice):
    data = dict()
    po_total_cost = 0
    time_total_cost = 0
    items = Item.objects.filter(purchase__work_order=invoice.work_order)
    tmcs = TimeCard.objects.filter(work_order=invoice.work_order)
    try:
       emplyee_name = request.user.em_user.name
    except Exception as e:
       emplyee_name = ""

    if request.user.preference.address:
       user_address = request.user.preference.address
    else:
      user_address = ""

    inv_form = '''
    {}
{}
    '''.format(emplyee_name,user_address).strip()

    for item in items:
        po_total_cost = po_total_cost + item.line_cost

    for card in tmcs:
        time_total_cost = time_total_cost + card.total_cost

    total_final = po_total_cost + time_total_cost

    if request.method == 'POST':
        if form.is_valid():
            doc_form = form.save()
            data['form_is_valid'] = True
            return redirect(redirect_to)

        else:
            data['form_is_valid'] = False
    context = {
    	'form': form,
    	'items': items,
    	'tmcs': tmcs,
    	'po_total_cost':po_total_cost,
    	'time_total_cost':time_total_cost,
    	'total_final':total_final,
    	'inv_form':inv_form
    	}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)



def invoice_update(request, pk):
    invoice = get_object_or_404(Invoice, pk=pk)
    if request.method == 'POST':
        form = InvoiceForm(request.POST,request.FILES or None, instance=invoice)
    else:
        form = InvoiceForm(instance=invoice)
    return invoice_render(request, form, 'invoice/update_invoice.html','/invoice/list/',invoice)



def invoice_approved(request,id):
	invoice = Invoice.objects.get(id=id)
	invoice.approved=True
	invoice.save()
	return redirect('/invoice/list/')

def invoice_paid(request,id):
	invoice = Invoice.objects.get(id=id)
	invoice.invoice_status="Paid"
	invoice.save()
	return redirect('/invoice/list/')

def invoice_sent(request,id):
	invoice = Invoice.objects.get(id=id)
	invoice.send_copy_tick_box=True
	invoice.invoice_status="Sent"
	invoice.save()
	return redirect('/invoice/list/')

def timecard_approved(request,id):
	timecard = TimeCard.objects.get(id=id)
	timecard.approved=True
	timecard.save()
	return redirect('/timecard/list/')

def purchase_approved(request,id):
	purchase = Purchase.objects.get(id=id)
	purchase.approved=True
	purchase.save()
	return redirect('/purchase/list/')

def workorder_delete(request,id):
	workorder = WorkOrder.objects.get(id=id)
	workorder.delete()
	return redirect('/workorder/list/')

def employee_delete(request,id):
	employee = Employee.objects.get(id=id)
	employee.delete()
	return redirect('/employee/list/')

def customer_delete(request,id):
	customer = Customer.objects.get(id=id)
	customer.delete()
	return redirect('/customer/list/')


def purchase_delete(request,id):
	purchase = Purchase.objects.get(id=id)
	purchase.delete()
	return redirect('/purchase/list/')

def timecard_delete(request,id):
	timecard = TimeCard.objects.get(id=id)
	timecard.delete()
	return redirect('/timecard/list/')

def documentation_delete(request,id):
	documentation = Documentation.objects.get(id=id)
	documentation.delete()
	return redirect('/documentation/list/')
def quote_delete(request,id):
	quote = Quote.objects.get(id=id)
	quote.delete()
	return redirect('/quote/list/')




def update_markup_percentage(request,item_id,pvalue):
	data = dict()
	item = Item.objects.get(id=item_id)
	qty = item.quantity
	cost = item.po_cost_ex_gst
	markup_percentage = float(pvalue)
	markup_amount = item.ma
	markup_gst = 10
	line_total_ex_gst = qty * cost * (1+markup_percentage/100) + markup_amount
	gst = line_total_ex_gst / 100 * markup_gst
	line_total = line_total_ex_gst + gst
	item.gst = gst
	item.line_cost = line_total
	item.mp = markup_percentage
	item.save()
	po_total_cost = 0
	time_total_cost = 0
	items = Item.objects.filter(purchase=item.purchase)
	for item in items:
		po_total_cost = po_total_cost + item.line_cost

	tmcs = TimeCard.objects.filter(work_order=item.purchase.work_order)

	for card in tmcs:
		time_total_cost = time_total_cost + card.total_cost

	total_final = po_total_cost + time_total_cost

	context = {
		"items":items,
		"tmcs":tmcs,
		"po_total_cost":po_total_cost,
		"total_final":total_final,
		"time_total_cost":time_total_cost
	}
	data['update_row'] = render_to_string("invoice/inv_table.html", context, request=request)
	return JsonResponse(data)

def update_markup_amount(request,item_id,avalue):
	data = dict()
	item = Item.objects.get(id=item_id)
	qty = item.quantity
	cost = item.po_cost_ex_gst
	markup_percentage = item.mp
	markup_amount = float(avalue)
	markup_gst = 10
	line_total_ex_gst = qty * cost * (1+markup_percentage/100) + markup_amount
	gst = line_total_ex_gst / 100 * markup_gst
	line_total = line_total_ex_gst + gst
	item.gst = gst
	item.line_cost = line_total
	item.ma = markup_amount
	item.save()
	po_total_cost = 0
	time_total_cost = 0
	items = Item.objects.filter(purchase=item.purchase)
	for item in items:
		po_total_cost = po_total_cost + item.line_cost


	tmcs = TimeCard.objects.filter(work_order=item.purchase.work_order)

	for card in tmcs:
		time_total_cost = time_total_cost + card.total_cost

	total_final = po_total_cost + time_total_cost


	context = {
		"items":items,
		"tmcs":tmcs,
		"po_total_cost":po_total_cost,
		"total_final":total_final,
		"time_total_cost":time_total_cost
	}
	data['update_row'] = render_to_string("invoice/inv_table.html", context, request=request)
	return JsonResponse(data)


def time_card_update_markup_percentage(request,time_id,pvalue):
	data = dict()
	timecard = TimeCard.objects.get(id=time_id)

	markup_percentage = float(pvalue)
	markup_amount = timecard.ma
	markup_gst = 10
	duration = timecard.duration
	chargeout_rate = timecard.employee_name.chargeout_rate
	total = duration * chargeout_rate * (1+markup_percentage/100) + markup_amount
	gst = total / 100 * markup_gst
	line_total = total + gst

	timecard.mp = markup_percentage
	timecard.gst = gst
	timecard.total_cost = line_total
	timecard.save()

	po_total_cost = 0
	time_total_cost = 0

	items = Item.objects.filter(purchase__work_order=timecard.work_order)
	for item in items:
		po_total_cost = po_total_cost + item.line_cost

	tmcs = TimeCard.objects.filter(work_order=timecard.work_order)

	for card in tmcs:
		time_total_cost = time_total_cost + card.total_cost

	total_final = po_total_cost + time_total_cost

	context = {
		"tmcs":tmcs,
		"items": items,
		"po_total_cost":po_total_cost,
		"total_final":total_final,
		"time_total_cost":time_total_cost
	}
	data['update_row'] = render_to_string("invoice/inv_table.html", context, request=request)
	return JsonResponse(data)


def time_card_update_markup_amount(request,time_id,tavalue):
	data = dict()
	timecard = TimeCard.objects.get(id=time_id)

	markup_percentage = timecard.mp
	markup_amount = float(tavalue)
	markup_gst = 10
	duration = timecard.duration
	chargeout_rate = timecard.employee_name.chargeout_rate
	total = duration * chargeout_rate * (1+markup_percentage/100) + markup_amount
	gst = total / 100 * markup_gst
	line_total = total + gst

	timecard.ma = markup_amount
	timecard.gst = gst
	timecard.total_cost = line_total
	timecard.save()

	po_total_cost = 0
	time_total_cost = 0

	items = Item.objects.filter(purchase__work_order=timecard.work_order)
	for item in items:
		po_total_cost = po_total_cost + item.line_cost

	tmcs = TimeCard.objects.filter(work_order=timecard.work_order)

	for card in tmcs:
		time_total_cost = time_total_cost + card.total_cost

	total_final = po_total_cost + time_total_cost

	context = {
		"tmcs":tmcs,
		"items": items,
		"po_total_cost":po_total_cost,
		"total_final":total_final,
		"time_total_cost":time_total_cost
	}
	data['update_row'] = render_to_string("invoice/inv_table.html", context, request=request)
	return JsonResponse(data)

class QuoteList(ListView):
	model = Quote
	template_name = 'quote/quote_list.html'
	context_object_name = 'quote_list'

	def get_context_data(self,**kwargs):
		data = super(QuoteList,self).get_context_data(**kwargs)
		data["quote_form"] = QuoteForm()
		data["quote_status"] = QUOTE_STATUS
		data['quotes_not_sent'] = self.model.objects.filter(quote_status="Quoting")
		today = datetime.datetime.today().date()
		past_due_date = self.model.objects.filter(quote_request_date__lt=today).count()
		data["past_due_date"] = past_due_date
		return data

class QuoteCreate(CreateView):
	model = Quote
	template_name = 'quote/quote_create.html'
	form_class = QuoteForm
	success_url = '/quote/list/'

	def form_invalid(self, form, *args, **kwargs):
		return JsonResponse(form.errors)

#quote update

def quote_render(request, form,form2,template_name,redirect_to,pk):
    data = dict()

    redirect_url = redirect_to+"?quote-"+pk
    if request.method == 'POST':
        if form.is_valid() and form2.is_valid():
            quote_form = form.save()
            with transaction.atomic():
                form2.instance = quote_form
                form2.save()
            return redirect(redirect_url)

        else:
            # data['form_is_valid'] = False
            # data['err'] = form2.errors
            # data['err2'] = form.errors
            return redirect(redirect_url)
    context = {
		'form': form,
		'form2': form2,
    }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)



def quote_update(request, pk):
	quote = get_object_or_404(Quote, pk=pk)
	if request.method == 'POST':
		form = QuoteForm(request.POST,request.FILES or None, instance=quote)
		form2 = QuoteDocumentationInlineForm(request.POST,request.FILES or None, instance=quote)
	else:
		form = QuoteForm(instance=quote)
		form2 = QuoteDocumentationInlineForm(instance=quote)

	return quote_render(request, form,form2, 'quote/update_quote.html','/quote/list/',pk)
