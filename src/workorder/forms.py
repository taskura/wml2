from django.forms import ModelForm, inlineformset_factory, TextInput
from .models import (
	WorkOrder,
	Employee,
	Customer,
	Purchase,
	Task,
	TimeCard,
	Documentation,
	Item,
	Invoice,
	Quote,
	QuoteDocument
	)

from django import forms



class WorkOrderForm(ModelForm):
	customer_contact_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	quote_sent_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	promised_delivery_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	class Meta:
		model = WorkOrder
		fields = [
			"customer",
			"description",
			"description_long_text",
			"custom_quote",
			"customer_contact_date",
			"quote_sent_date",
			"revision",
			"promised_delivery_date",
			"job_status",

		]

class QuoteForm(ModelForm):
	customer_contact_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	# quote_sent_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	quote_request_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	promised_delivery_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	class Meta:
		model = Quote
		fields = [
			"customer",
			"description",
			"description_long_text",
			"customer_contact_date",
			"quote_request_date",
			"promised_delivery_date",
			"custom_quote",
			"quote_amount",
			"quote_status",

		]

class QuoteUpdateForm(ModelForm):
	customer_contact_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	quote_sent_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	quote_request_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	promised_delivery_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	class Meta:
		model = Quote
		fields = [
			"customer",
			"description",
			"description_long_text",
			"customer_contact_date",
			"quote_request_date",
			"promised_delivery_date",
			"quote_sent_date",
			"quote_amount",
			"quote_status",

		]


class EmployeeForm(ModelForm):
	start_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"], required=False)
	class Meta:
		model = Employee
		fields = [
			"user",
			"external_system",
			"name",
			"email",
			"start_date",
			"photo",
			"address",
			"phone_number",
			"emergency_contact",
			"emergency_contact_relationship",
			"emergency_contact_number",
			"emergency_contact_email",
			"cost_rate",
			"chargeout_rate"
		]



class EmployeeUpdateForm(ModelForm):
	start_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"], required=False)
	terminate_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"], required=False)
	class Meta:
		model = Employee
		fields = [
			"user",
			"external_system",
			"name",
			"email",
			"start_date",
			'terminate_date',
			'terminated',
			"photo",
			"address",
			"phone_number",
			"emergency_contact",
			"emergency_contact_relationship",
			"emergency_contact_number",
			"emergency_contact_email",
			"cost_rate",
			"chargeout_rate"
		]


class EmployeeProfileUpdateForm(ModelForm):
	class Meta:
		model = Employee
		fields = [
			"email",
			"address",
			"phone_number",
			"emergency_contact",
			"emergency_contact_relationship",
			"emergency_contact_number",
			"emergency_contact_email",
		]


class CustomerForm(ModelForm):
	class Meta:
		model = Customer
		fields = '__all__'








class TimeCardForm(ModelForm):
	start_time = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	end_time = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	class Meta:
		model = TimeCard
		fields = ["start_time","end_time","employee_name","work_order"]








class PurchaseForm(ModelForm):
	required_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	promised_delivery_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"], required=False)
	class Meta:
		model = Purchase
		fields = [
			"work_order",
			"po_status",
			"vendor",
			"required_date",
			"promised_delivery_date",
		]



class InvoiceForm(ModelForm):
	invoice_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"])
	class Meta:
		model = Invoice
		fields = [
			"invoice_date",
			# "expected_payment_date",
			"invoice_to",
			"invoice_from_org",
			"invoice_from",
			"invoice_customer_info",
			]








class ItemInlineForm(ModelForm):
	received_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"], required=False)
	class Meta:
		model = Item
		fields = [
			"purchase",
			"line_text",
			"quantity",
			"received_qty",
			"received_date",
			"po_cost_ex_gst",
			"approved",
		]

class TaskForm(ModelForm):
	start_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"], required=False)
	end_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"], required=False)
	task_due_date = forms.DateTimeField(input_formats=["%d/%m/%Y %H:%M"], required=False)
	class Meta:
		model = Task
		fields = [
			"work_order",
			"task_description",
			"start_date",
			"end_date",
			"task_status",
			"task_assigned_to",
			"task_due_date",
		]

class DocumentationForm(ModelForm):
	class Meta:
		model = Documentation
		fields = '__all__'


class QuoteDocumentationForm(ModelForm):
	class Meta:
		model = QuoteDocument
		fields = '__all__'

PurchaseItemInlineForm = inlineformset_factory(Purchase, Item,form=ItemInlineForm, can_delete=True, extra=0,min_num=1)
TaskItemInlineForm = inlineformset_factory(WorkOrder, Task,form=TaskForm, can_delete=True, extra=0,min_num=1)
DocumentationInlineForm = inlineformset_factory(WorkOrder, Documentation,form=DocumentationForm, can_delete=True, extra=0,min_num=1)
QuoteDocumentationInlineForm = inlineformset_factory(Quote, QuoteDocument,form=QuoteDocumentationForm, can_delete=True, extra=0,min_num=1)
