from django.contrib import admin

# Register your models here.
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.conf import settings
from datetime import datetime, timedelta
from .models import (
	Customer,
	WorkOrder,
	Documentation,
	Employee,
	Task,
	TimeCard,
	Purchase,
	Invoice,
	Item,
	Preference,
	DocumentationCategory,
	Quote

	)

import inspect

def current_user():
	for frame_record in inspect.stack():
		if frame_record[3]=='get_response':
			request = frame_record[0].f_locals['request']
			break
		else:
			request = None
	return request.user

#RESOURCE ADMIN CLASSES
class AdminUserResource(resources.ModelResource):
    class Meta:
        model = User
    def import_obj(self, obj, data, dry_run):
        """
        Traverses every field in this Resource and calls
        :meth:`~import_export.resources.Resource.import_field`.
        """
        for field in self.get_fields():
            if field.column_name == 'password':
                passwd = make_password(data.get('password'))
                data.update({'password': passwd})
            self.import_field(field, obj, data)


class WorkOrderResource(resources.ModelResource):
    class Meta:
        model = WorkOrder
        exclude = ('created_by','modified_by','is_template','all_tasks_complete')

class DocumentationResource(resources.ModelResource):
    class Meta:
        model = Documentation

class EmployeeResource(resources.ModelResource):
    class Meta:
        model = Employee
        exclude = ('terminated','photo')

class TaskResource(resources.ModelResource):
    class Meta:
        model = Task
        exclude = ('task_duration')

class PurchaseResource(resources.ModelResource):
    class Meta:
        model = Purchase

class DocumentationCategoryResource(resources.ModelResource):
    class Meta:
        model = DocumentationCategory


class TimeCardResource(resources.ModelResource):
    class Meta:
        model = TimeCard
        exclude = ('approved_date')
    def import_obj(self, obj, data, dry_run):
        cuser = current_user()
        for field in self.get_fields():
            chargeout_rate = Employee.objects.get(id=data.get('employee_name')).chargeout_rate
            markup_percentage = cuser.preference.markup_percentage
            markup_amount = cuser.preference.markup_amount
            markup_gst = 10
            end_date = datetime.strptime(data.get('end_time'), "%Y-%m-%d %H:%M:%S")
            start_date = datetime.strptime(data.get('start_time'), "%Y-%m-%d %H:%M:%S")
            day_between = end_date - start_date
            duration = day_between.days * 24 + day_between.seconds // 3600
            chargeout_rate = chargeout_rate
            total = duration * chargeout_rate * (1+markup_percentage/100) + markup_amount
            gst = total / 100 * markup_gst
            line_total = total + gst
            # data.get('password')
            data.update({
			'duration': duration,
			'total_cost': line_total,
			'mp': markup_percentage,
			'ma': markup_amount,
			'gst': gst
			})
            self.import_field(field, obj, data)


class QuoteResource(resources.ModelResource):
    class Meta:
        model = Quote
        exclude = ('created_date','modified_date','quote_sent_date')



class CustomerResource(resources.ModelResource):
    class Meta:
        model = Customer
        exclude = ()


class PreferenceResource(resources.ModelResource):
    class Meta:
        model = Preference
        exclude = ('logo')



#MODEL ADMIN
class DocumentationCategoryAdmin(ImportExportModelAdmin):
    resource_class = DocumentationCategoryResource


class WorkOrderAdmin(ImportExportModelAdmin):
    resource_class = WorkOrderResource

class AdminUserAdmin(ImportExportModelAdmin):
    resource_class = AdminUserResource

class DocumentationAdmin(ImportExportModelAdmin):
    resource_class = DocumentationResource


class EmployeeAdmin(ImportExportModelAdmin):
    resource_class = EmployeeResource

class TaskAdmin(ImportExportModelAdmin):
    resource_class = TaskResource

class PurchaseAdmin(ImportExportModelAdmin):
    resource_class = PurchaseResource

class TimeCardAdmin(ImportExportModelAdmin):
    resource_class = TimeCardResource

class QuoteAdmin(ImportExportModelAdmin):
    resource_class = QuoteResource

class CustomerAdmin(ImportExportModelAdmin):
    resource_class = CustomerResource

class PreferenceAdmin(ImportExportModelAdmin):
    resource_class = PreferenceResource



# admin.site.unregister(User)
# admin.site.register(User, AdminUserAdmin)


admin.site.register(Customer, CustomerAdmin)
admin.site.register(WorkOrder, WorkOrderAdmin)
admin.site.register(Documentation, DocumentationAdmin)
admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Task, TaskAdmin)
admin.site.register(TimeCard, TimeCardAdmin)
admin.site.register(Purchase, PurchaseAdmin)
admin.site.register(Invoice)
admin.site.register(Item)
admin.site.register(Preference, PreferenceAdmin)
admin.site.register(DocumentationCategory, DocumentationCategoryAdmin)
admin.site.register(Quote, QuoteAdmin)
