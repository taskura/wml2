var wmlloadForm = function () {
    var btn = $(this);

    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#editcontainer").html("");

      },
      success: function (data) {
        $("#editcontainer").html(data.html_form);
        $("#updateModal").modal('show')

      }
    });
  };

  var pattern = /[?&]workworder-/;
  var URL3 = location.search;

  if(pattern.test(URL3)){
      $.ajax({
        url: "/workorder/update/"+URL3.substring(12)+"/",
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
          $("#editcontainer").html("");

        },
        success: function (data) {
          $("#editcontainer").html(data.html_form);
          $("#updateModal").modal('show')

        }
      });

  }

  var pattern = /[?&]quote-/;
  var URL3 = location.search;

  if(pattern.test(URL3)){
      $.ajax({
        url: "/quote/update/"+URL3.substring(7)+"/",
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
          $("#editcontainer").html("");

        },
        success: function (data) {
          $("#editcontainer").html(data.html_form);
          $("#updateModal").modal('show')

        }
      });

  }




$("[id='doc_edit']").click(wmlloadForm);
$("[id='timecard_edit']").click(wmlloadForm);
$("[id='customer_edit']").click(wmlloadForm);
$("[id='employee_edit']").click(wmlloadForm);
$("[id='workorder_edit']").click(wmlloadForm);
$("[id='purchase_edit']").click(wmlloadForm);
$("[id='purchase_create']").click(wmlloadForm);
$("[id='invoice_edit']").click(wmlloadForm);
$("[id='work_to_invoice']").click(wmlloadForm);
$("[id='quote_edit']").click(wmlloadForm);


function get_active(path){
if (path.match(/customer/g)) {
    $('#id_customer').addClass('xactive');
  }
  else if (path.match(/employee/g)) {
    $('#id_employee').addClass('xactive');
  }
  else if (path.match(/quote/g)) {
    $('#id_quote').addClass('xactive');
  }
  else if (path.match(/purchase/g)) {
    $('#id_purchase').addClass('xactive');
  }
  else if (path.match(/task/g)) {
    $('#id_task').addClass('xactive');
  }
  else if (path.match(/timecard/g)) {
    $('#id_timecard').addClass('xactive');
  }
  else if (path.match(/documentation/g)) {
    $('#id_documentation').addClass('xactive');
  }
  else if (path.match(/invoice/g)) {
    $('#id_invoice').addClass('xactive');
  }
  else{
    $('#id_workorder').addClass('xactive');
  }
}


$(document).ready(function() {
  get_active(window.location.pathname.split( '/' )[1]);

});

$("#t_start_time").datetimepicker({format:'d/m/Y H:i'});

$("#t_end_time").datetimepicker({format:'d/m/Y H:i'});

$("#start_date").datetimepicker({format:'d/m/Y H:i'});

$("#id_customer_contact_date").datetimepicker({format:'d/m/Y H:i'});

$("#id_quote_sent_date").datetimepicker({format:'d/m/Y H:i'});

$("#id_promised_delivery_date").datetimepicker({format:'d/m/Y H:i'});
$("#id_quote_request_date").datetimepicker({format:'d/m/Y H:i'});


$("#employee_update_btn").click(function(){
  $("#employee_info_box").hide();
  $("#employee_update_form").show();

})
$("#employee_update_close_btn").click(function(){
  $("#employee_info_box").show();
  $("#employee_update_form").hide();

})

$("[id='workworder_status_dropdown_id']").change(function(e) {
  var obj = $(this)
  $.ajax({
        type:'POST',
        url:obj.attr("data-url"),
        data:{
          new_status:obj.val(),
          csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
        },
        success:function(data){
        }
      });

      e.preventDefault();
});


$("[id='quote_status']").change(function(e) {
  var obj = $(this)
  $.ajax({
        type:'POST',
        url:obj.attr("data-url"),
        data:{
          new_status:obj.val(),
          csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
        },
        success:function(data){
        }
      });

      e.preventDefault();
});

// var abc;

function edu(obj){
  var db_id = $(obj).attr("dbid")
  $("#"+obj.id).addClass("helooo" )
  $("#editable"+db_id).show()
  var text = $.trim($("#mainvalue"+db_id).text())
  $("#edittext"+db_id).val(text)
}

function removebtn(obj){
  var db_id = $(obj).attr("dbid")
  $("#editable" + db_id).hide()
  $("#mainvalue"+ db_id).removeClass("helooo" )

}


function addbtn(obj){
  var db_id = $(obj).attr("dbid")
  $("#editable" + db_id).hide()
  var new_text = $("#edittext"+db_id).val()
  $("#mainvalue"+ db_id).text(new_text)
  $("#mainvalue"+ db_id).removeClass("helooo")

  $.ajax({
        type:'POST',
        url:$(obj).attr("data-url"),
        data:{
          new_text:new_text,
          csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
        },
        success:function(data){
        }
      });

}

$( "#workcreate" ).submit(function( event ) {
  var btn = $(this)
  event.preventDefault();
  $.ajax({
        url: btn.attr("action"),
        data: btn.serialize(),
        type: btn.attr("method"),
        success:function(data){
          if (data.custom_quote) {
            var err = data.custom_quote[0]
            $("#errmsg").text(err)
          }else{
            location.reload();
          }

        },
        error: function (xhr, ajaxOptions, error, data) {
            alert(xhr.status);
            console.log(ajaxOptions);
            console.log(error);
            console.log(data);
        }
      });

});


$("[id='promised_delivery_date']").datetimepicker({
  format:'d/m/Y H:i',
  onClose:function(ct,$input){
    var new_date = $input.val()
    if (new_date == "") {
    }else {
      f_date = ct.dateFormat('Y-m-d H:i')
      $.ajax({
            type:'POST',
            url:$input.attr("data-url"),
            data:{
              new_date:f_date,
              csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
            },
            success:function(data){

            }
          });
    }

  }
});
