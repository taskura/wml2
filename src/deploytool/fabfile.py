"""
fabfile.py

Use the fabric api to ease the deploy process. It doesn't solve migration issues :(
"""
from fabric.contrib.files import exists, sed, sudo
from fabric.context_managers import prefix
from fabric.api import run, env, cd, parallel
import os

env.user = 'ubuntu'
env.hosts = [
    'ec2-18-223-98-63.us-east-2.compute.amazonaws.com'
]
env.key_filename = './wml.pem'
env.home = '/home/ubuntu'
env.venv = os.path.join(env.home)
env.forward_agent = True
env.gitdir = os.path.join(env.home, 'wml', 'src')

def activate_venv(fn):
    """
    Decorator to activate the virtual environment
    """
    def inner(*args, **kwargs):
        with prefix('cd && source env/bin/activate'):
            fn(*args, **kwargs)
    return inner

def cdgitdir(fn):
    """
    Decorator to go to the git repository
    """
    def inner(*args, **kwargs):
        with prefix('cd %s' % env.gitdir):
            fn(*args, **kwargs)
    return inner

@parallel
def install_packages():
    """
    Install all required system packages
    """
    run('sudo apt-get install -y %s ' % ' '.join([
        'python3-pip', 'nginx', 'python3-dev', 'libmysqlclient-dev', 'gettext'
    ]))

@parallel
@activate_venv
@cdgitdir
def install_pip_requirements():
    """
    Install pip requirements of tot app
    """
    run('pip install --upgrade pip')
    run('pip install -r requirements.txt')

@parallel
@activate_venv
@cdgitdir
def collectstatic():
    """
    Collects static file into s3 bucket
    """
    run('python manage.py collectstatic --no-input')

@parallel
@activate_venv
@cdgitdir
def compilemessages():
    """
    Compile .po files for translation engine
    """
    run('python manage.py compilemessages')

@activate_venv
@cdgitdir
def migrate():
    """
    Migrate the database
    """
    run('python manage.py migrate')
    load_fixtures()

@activate_venv
@cdgitdir
def load_fixtures():
    """
    Load necessary data for the app to be alive
    """
    run('find . -name "*fixtures*" -type f -printf "%p "\
     | xargs python manage.py loaddata')

@cdgitdir
def update_repo():
    """
    Fetch the latest version of work branch in our aws servers
    """
    run('git checkout -- .')
    run('git reset --hard HEAD')
    run('git checkout master')
    run('git pull')

def get_latest_version():
    """
    Update our servers with latests version
    """
    update_repo()
    # install_packages()
    install_pip_requirements()
    collectstatic()
    # compilemessages()
    migrate()

@parallel
def restart_server():
    """
    Restart nginx and gunicorn for changes to be visible
    """
    sudo('systemctl restart gunicorn')
    # sudo('systemctl restart gunicorn-secure')

@parallel
def clean():
    """
    Clean all pyc files
    """
    run('find . -name ".pyc" -exec rm -rf {} +')

def deploy():
    """
    Deploy to our aws servers
    """
    get_latest_version()
    restart_server()
